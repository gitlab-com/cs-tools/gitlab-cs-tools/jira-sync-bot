from gidgetlab.aiohttp import GitLabBot
import gidgetlab
import requests
import os
import xml.etree.ElementTree as ET
import argparse
import sys

# todo:
 # logging
 # have a set queue for the sync reqests

mr_router = gidgetlab.routing.Router()
push_router = gidgetlab.routing.Router()
jira_user = ""
jira_pw = ""
jira_url = ""
repomap = {}

@mr_router.register("Merge Request Hook")
async def mr_event(event, gl, *args, **kwargs):
    """Sync MR events to Jira https://confluence.atlassian.com/jirakb/delays-for-commits-to-display-in-development-panel-in-jira-server-779160823.html"""
    is_in_map = sync_repo(event)
    if not is_in_map:
        print("Could not find Jira repository ID for %s. Updating Jira repo map." % (event.data["project"]["path_with_namespace"]), file=sys.stderr)
        second_sync = sync_repo(event, True)
        if not second_sync:
            print("%s not in Jira repos. Try updating Jira DVCS repositories." % (event.data["project"]["path_with_namespace"]), file=sys.stderr)
    pass

@push_router.register("Push Hook")
async def push_event(event, gl, *args, **kwargs):
    """Sync Push events to Jira https://confluence.atlassian.com/jirakb/delays-for-commits-to-display-in-development-panel-in-jira-server-779160823.html"""
    is_in_map = sync_repo(event)
    if not is_in_map:
        print("Could not find Jira repository ID for %s. Updating Jira repo map." % (event.data["project"]["path_with_namespace"]), file=sys.stderr)
        second_sync = sync_repo(event, True)
        if not second_sync:
            print("%s not in Jira repos. Try updating Jira DVCS repositories." % (event.data["project"]["path_with_namespace"]), file=sys.stderr)
    pass

def sync_repo(event, update = False):
    project_ns = event.data["project"]["path_with_namespace"]

    event_type = event.data["object_kind"]
    content_type = "application/json"
    if event_type == "push":
        content_type = "application/x-www-form-urlencoded"
    elif event_type == "merge_request":
        content_type = "application/json"

    if update:
        update_repomap(jira_url, jira_user, jira_pw)

    is_in_map = False
    for repo in repomap.keys():
        # jira repo names contain group namespace for projects in subgroups, but don't contain it for projects directly in the top group
        # because of this we check if the project triggering the webhook (with complete path from jira) ends with the potentially incomplete repo name jira knows
        if project_ns.endswith(repo):
            jira_repo_id = repomap[repo]
            jira_repo_url =  "%srest/bitbucket/1.0/repository/%s/sync" % (jira_url, jira_repo_id)
            header = {"Content-Type" : content_type}
            response = requests.post(jira_repo_url, headers=header)
            if response.status_code == 200:
                print("Synced %s for %s successfully." % (event_type, project_ns))
            else:
                print("Failed syncing %s for %s : %s" % (event_type, project_ns, response.status_code), file=sys.stderr)
            #log this properly: 200 success, else error with response status
            is_in_map = True
            break
    return is_in_map

def update_repomap(jira_url, jira_user, jira_pw):
    # parse http://$jira/rest/bitbucket/1.0/repositories XML to get repo ids
    response = requests.get(jira_url + "rest/bitbucket/1.0/repositories", auth=(jira_user, jira_pw))

    #parse map of {$gitlab_project_path : $jira_repo_id} from XML
    root = ET.fromstring(response.text)
    repomap.clear()
    for repo in root.findall("./repositories"):
        jira_id = repo.find("id").text
        project_path = repo.find("name").text.replace("@","/")
        repomap[project_path] = jira_id
    return repomap

parser = argparse.ArgumentParser()
parser.add_argument("git_user", help="Name of the bot user")
parser.add_argument("git_token", help="GitLab token")
parser.add_argument("git_secret", help="GitLab webhook secret token for validation")
parser.add_argument("git_url", help="GitLab instance URL")
parser.add_argument("jira_user", help="Jira username")
parser.add_argument("jira_pw", help="Jira password")
parser.add_argument("jira_url", help="Jira instance URL")
parser.add_argument("--port","-p", help="Port the bot should run on", default=8080)
args = parser.parse_args()

jira_user = args.jira_user
jira_pw = args.jira_pw
jira_url = args.jira_url if args.jira_url.endswith("/") else args.jira_url + "/"

update_repomap(jira_url, jira_user, jira_pw)

bot = GitLabBot(args.git_user, secret=args.git_secret,
    access_token=args.git_token,
    url=args.git_url)

bot.register_routers(mr_router, push_router)
bot.run(port=args.port)

