# jira-sync-bot

A bot that facilitates real-time updates in Jira DVCS console by relaying GitLab webhooks to Jira's weird BitBucket API.

This project runs a small Python server that listens to GitLab webhooks. It queries Jira DVCS repositories to get a map of `{$gitlab_project_path : $jira_repo_id}`. When a GitLab webhook is triggered, it uses the event data to identify if it is a `push` or `MR` event. On `push` it calls the `$jira_url/rest/bitbucket/1.0/repository/$jira_repo_id/sync` endpoint with `content-type: application/x-www-form-urlencoded`. On `MR` it calls the endpoint with `content-type: application/json`. 

Because jira-sync-bot figures out which GitLab repo maps to which Jira DVCS repo, GitLab webhooks can be configured on group level. Only the changed repo is synced in Jira to keep load on Jira minimal.

**Testing was done on a local instance. No stability or long-term testing has been performed, so this may be unstable, especially in case of higher request volume**

```mermaid
sequenceDiagram
    participant User
    participant GitLab
    participant Bot as Jira Sync Bot
    participant Jira
    User->>GitLab: Push code / edit MR
    GitLab->>Bot: Trigger Push / MR webhook event
    Bot->>Jira: Request DVCS repo list
    Jira-->>Bot: Send DVCS repo list
    Note right of Bot: Bot: Find repo in list
    Bot->>Jira: Trigger Push / Repo Sync
    Jira->>GitLab: Request commit / MR data
    GitLab-->>Jira: Send commit / data
```

## Setup

* Setup Jira DVCS accounts
  * https://docs.gitlab.com/ee/integration/jira_development_panel.html
* Setup a webhook for an instance / group / project you want to run this on:
  * Follow this guide: https://docs.gitlab.com/ee/user/project/integrations/webhooks.html
  * URL of the webhook should be host this bot runs on and configured port
  * Secret token you configure in the web hook = the $GIT_SECRET parameter to start the bot
  * Trigger for push events and Merge request events
  * Don't enable SSL verification
  * add the webhook
* Run the bot
* Upvote this issue: https://gitlab.com/gitlab-org/gitlab/-/issues/3359

## Usage

`python3 jira_sync_bot.py $GIT_USER $GIT_TOKEN $GIT_SECRET $GIT_URL $JIRA_USER $JIRA_PW $JIRA_URL`

Arguments:
  * `$GIT_USER`: The user this bot will act as
  * `$GIT_TOKEN`: This user's GitLab API token with read_api scope
  * `$GIT_SECRET`: The secret token configured in the GitLab webhook used for request validation
  * `$GIT_URL`: The URL of the GitLab instance
  * `$JIRA_USER`: The Jira user this bot will use to request the Jira API
  * `$JIRA_PW`: The Jira user's PW this bot will use to request the Jira API
  * `$JIRA_URL`: The URL of the Jira instance

optional parameter:
  * `-p`, `--port`: state the port the bot runs on, defaults to 8080

## Disclaimer

This script is provided for educational purposes. It is not supported by GitLab. However, you can create an issue if you need help or better propose a Merge Request. Before using this script in production, check the consistency of the results on a test instance, and of course make backups.
